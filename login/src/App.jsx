import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";
// components
import Login from "./components/Login";
import Header from 'home/Header';

const App = () => (
  <div className="mt-10 text-3xl mx-auto max-w-6xl">
    <Header />
    <Login />
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
