import React, { useEffect } from 'react';
import errForm from '../../const/errorForm';
import { UploadOutlined } from '@ant-design/icons';
import { Modal, Upload, Button, Form } from 'antd';
import getImageFile from '../../utility/getImageFile';

function ModalImport({ modalTitle, handleOk, handleCancle, visible }) {
  const [form] = Form.useForm();

  useEffect(() => {
    visible && form.resetFields();
  }, [form, visible]);

  const beforeUploadLogo = () => {
    return false;
  };

  const handleSubmit = () => {
    form.validateFields().then(async (values) => {
      let importFile = null;
      if (values.file) {
        importFile = values.file[0].originFileObj;
      }
      const formData = new FormData();
      formData.append('file', importFile);
      handleOk(formData);
    });
  };

  return (
    <Modal
      onOk={handleSubmit}
      visible={visible}
      title={modalTitle}
      onCancel={handleCancle}
    >
      <Form form={form} size="large" name="company_form">
        <Form.Item
          name="file"
          valuePropName="fileList"
          getValueFromEvent={getImageFile}
          rules={[
            {
              required: true,
              message: errForm.required,
            },
          ]}
        >
          <Upload maxCount={1} listType="text" beforeUpload={beforeUploadLogo}>
            <Button className="mb-2" icon={<UploadOutlined />}>
              Click to upload
            </Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default ModalImport;
