import '@css/modal.css';

function MainModal(props) {
  return (
    <>
      <div className="overlay for-recom"></div>
      <div className="popup">
        <div className="popup-head">Are You Over 18?</div>
        <div className="popup-con">
          <strong>store tada</strong>, requires you to confirm your age before
          ordering. Venue staff may request proof of age.
        </div>
        <div className="popup-foot">
          <ul className="lay-2">
            {props.component.secondaryButton}
            {props.component.primaryButton}
          </ul>
        </div>
      </div>
    </>
  );
}

export default MainModal;
