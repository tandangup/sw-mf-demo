import MainModal from './parts/MainModal';
import { PrimaryModalButton, SecondaryModalButton } from './parts/ModalButton';
function ModalAges() {
  return (
    <MainModal
      component={{
        primaryButton: (
          <PrimaryModalButton>Yes, I'm over 18</PrimaryModalButton>
        ),
        secondaryButton: (
          <SecondaryModalButton> No, I'm underage </SecondaryModalButton>
        ),
      }}
    />
  );
}

export default ModalAges;
