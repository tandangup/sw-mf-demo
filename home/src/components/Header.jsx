import React from 'react';

export default function Header() {
  return ( 
    <div>
      <div className="drop-shadow-xl h-24 bg-white text-center content-center grid">
        <div>
          <h6 className="text-xs text-gray-500"> Hujan emas road </h6>
        </div>
      </div>
      <div className="w-full h-auto">
        <img
          alt="Merchant logos"
          className="w-full"
          width={100}
          height={100}
          src="https://storage.googleapis.com/skipque3/settings/smartweb_login_screen_1609299915_ikea-logo-new-sq-1.jpg"
        />
      </div>
    </div>
    )
}