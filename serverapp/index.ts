import express from "express";

const app = express();
const port = 8080;

app.get("/", (req, res) => {
  res.send("Hello from serverapp");
});

app.listen(port, () => {
  console.log(`serverapp listening at http://localhost:${port}`);
});
